﻿using UnityEngine;
using System.Collections;

public class Animacion : MonoBehaviour {
	public Animator anim;
	public int Level;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		anim.SetInteger ("Level",Level );
		if(Level == 0){		
			anim.SetBool ("Play",true );
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
