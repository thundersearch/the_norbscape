﻿using UnityEngine;
using System.Collections;

public class AnimatorMenu : MonoBehaviour {
	public Animator anima;
	public bool isShowO = false;
	public bool isShowNe = false;
	public bool isShowL = false;
	public bool isShowNa = false;


	private static AnimatorMenu ani  = null;
	public static AnimatorMenu aniInstance {
		get{
			if(ani ==null){
				GameObject questObject = new GameObject ("Defaul");
				ani = questObject.AddComponent<AnimatorMenu> ();
			}

			return ani;
		}


	}

	void Awake(){
		if(ani){
			DestroyImmediate (gameObject);
			return;
		}
		ani = this;
		DontDestroyOnLoad (ani);
	}



	void Start () {	
		anima = this.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void update () {
	
	}

	public void showOption(bool x){
		isShowO=x;

		anima.SetBool ("isShowOption", isShowO);	
		
		
	}

	public void showNew(bool x){
		isShowNe=x;
		anima.SetBool ("isShowNew", isShowNe);	
		
	}

	public void showLoad(bool x){
		isShowL = x;

		anima.SetBool ("isShowLoad", isShowL);	
		
	}
	public	void showName(bool x){

			isShowNa=x;
			anima.SetBool ("isShowName", isShowNa);	
		
	}


}
