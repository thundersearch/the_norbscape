﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class Blueprint {

	public List<int> Ingredients = new List<int> ();
	public Item finalItem;
	public int XP;

	public Blueprint(List<int> ingredients,Item item,int xp){
		this.Ingredients = ingredients;
		this.finalItem = item;
		this.XP = xp;
	}

}
