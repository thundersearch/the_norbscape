﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class BlueprintDatabase : MonoBehaviour {
	public List<Blueprint> Blueprints = new List<Blueprint> ();
	ItemDatabase database;


	private static BlueprintDatabase Blue  = null;
	public static BlueprintDatabase BlueInstance {
		get{
			if(Blue ==null){
				GameObject BlueObject = new GameObject ("Defaul");
				Blue = BlueObject.AddComponent<BlueprintDatabase> ();
			}

			return Blue;
		}
	}
	void Awake(){
		if(Blue){
			DestroyImmediate (gameObject);
			return;
		}
		Blue = this;
		DontDestroyOnLoad (Blue);
	}


	// Use this for initialization
	void Start () {
		
		database = FindObjectOfType<ItemDatabase>();
		Blueprints.Add (new Blueprint(new List<int>(new int[]{95,95,45}),database.FetchItemByID(119),200));
		Blueprints.Add (new Blueprint(new List<int>(new int[]{45}),database.FetchItemByID(118),100));
		Blueprints.Add (new Blueprint(new List<int>(new int[]{79,79,105,105,105,105,105,105,105}),database.FetchItemByID(191),150));
		Blueprints.Add (new Blueprint(new List<int>(new int[]{191,44}),database.FetchItemByID(188),1500));
		Blueprints.Add (new Blueprint(new List<int>(new int[]{188,103}),database.FetchItemByID(186),10000));
		Blueprints.Add (new Blueprint(new List<int>(new int[]{103}),database.FetchItemByID(104),500));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
