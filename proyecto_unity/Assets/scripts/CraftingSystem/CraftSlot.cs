﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CraftSlot : MonoBehaviour,IDropHandler{
	public Item item;
	CraftSystem craftsystem;
	//Inventory inventory;
	// Use this for initialization
	void Start () {
		craftsystem = FindObjectOfType<CraftSystem> ();
		//inventory = FindObjectOfType<Inventory> ();
			
	}
		
	public void OnDrop (PointerEventData eventData)
	{ 
		if(item.ID == -1){
				
			ItemData droppedItem = eventData.pointerDrag.GetComponent<ItemData> ();
			Debug.Log ("Tipo del item " + droppedItem.item.Type);
			item = droppedItem.item;

			GameObject itemObj = Instantiate (craftsystem.itemCraft);
			itemObj.GetComponent<ItemDataCraft> ().item = item;
			itemObj.transform.SetParent (this.transform);
			itemObj.transform.position = this.transform.position;
			itemObj.GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);
			itemObj.GetComponent<Image> ().sprite = item.Sprite;
			itemObj.name = item.Title;
			craftsystem.ListWithItem ();


		}


	

	}


}
