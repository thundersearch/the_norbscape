﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class CraftSystem : MonoBehaviour {
	
	public GameObject itemCraft;
	public GameObject itemResult;
	public GameObject itemObj;
	int idResult=0;
	public List<CraftSlot> slots = new List<CraftSlot>();
	public List<int> itemID = new List<int>();

	BlueprintDatabase blueprintdatabase;
	private GameObject craftpanel;

	public List<Item> posiblesItems= new List<Item>();
	public List<int> idblueprint= new List<int>();

	// Use this for initialization
	void Start () {
		craftpanel =  GameObject.Find ("CraftingPanel");
		for(int i=0;i<9;i++){
			slots.Add (craftpanel.transform.GetChild(i).GetComponent<CraftSlot>());
		}
		blueprintdatabase = FindObjectOfType<BlueprintDatabase> ();
		craftpanel.SetActive (false);
	}
	
	// Update is called once per frame
	public void ListWithItem(){
		
		itemID.Clear ();
		posiblesItems.Clear ();	
		idblueprint.Clear ();
		idResult = 0;
		if(itemObj != null){
			Destroy (itemObj);
		}
	
		for(int i =0; i<slots.Count ; i++){
			if(slots[i].item.ID != -1){
				
				itemID.Add (slots[i].item.ID);				
			}
			
		}


		for(int k=0; k < blueprintdatabase.Blueprints.Count; k++){
			int aoumtOfTrue = 0;
			for(int z= 0; z<blueprintdatabase.Blueprints[k].Ingredients.Count; z++){
				
				for(int d=0;d< itemID.Count;d++){
					if(blueprintdatabase.Blueprints[k].Ingredients[z] == itemID[d]){
						aoumtOfTrue++;
						break;
					}
				} 
				if(aoumtOfTrue == blueprintdatabase.Blueprints[k].Ingredients.Count){
					posiblesItems.Add (blueprintdatabase.Blueprints[k].finalItem);	
					idblueprint.Add(k);

				}
			}
		}

		if(posiblesItems.Count>0){
			itemObj = Instantiate (itemResult);
			itemObj.GetComponent<ResultScript> ().item = posiblesItems[idResult];
			itemObj.GetComponent<ResultScript> ().xp = blueprintdatabase.Blueprints[idblueprint[idResult]].XP;
			itemObj.GetComponent<ResultScript> ().ingredients = blueprintdatabase.Blueprints[idblueprint[idResult]].Ingredients;
			itemObj.transform.SetParent (GameObject.FindGameObjectWithTag("Finish").transform);
			itemObj.transform.position = GameObject.FindGameObjectWithTag("Finish").transform.position;
			itemObj.GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);
			itemObj.GetComponent<Image> ().sprite = posiblesItems[idResult].Sprite;
			itemObj.name = posiblesItems[idResult].Title;
		}
	}

	public void nextItem(){
		idResult++;
		if(idResult>(posiblesItems.Count-1)){
			idResult = 0;
		}
		if(itemObj != null){
			Destroy (itemObj);
			itemObj = Instantiate (itemResult);
			itemObj.GetComponent<ResultScript> ().item = posiblesItems[idResult];
			itemObj.GetComponent<ResultScript> ().xp = blueprintdatabase.Blueprints[idblueprint[idResult]].XP;
			itemObj.GetComponent<ResultScript> ().ingredients = blueprintdatabase.Blueprints[idblueprint[idResult]].Ingredients;
			itemObj.transform.SetParent (GameObject.FindGameObjectWithTag("Finish").transform);
			itemObj.transform.position = GameObject.FindGameObjectWithTag("Finish").transform.position;
			itemObj.GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);
			itemObj.GetComponent<Image> ().sprite = posiblesItems[idResult].Sprite;
			itemObj.name = posiblesItems[idResult].Title;
		}

	}
	public void previousItem(){
		idResult--;
		if(idResult<0){
			idResult = posiblesItems.Count-1;
		}
		if(itemObj != null){
			Destroy (itemObj);
			itemObj = Instantiate (itemResult);
			itemObj.GetComponent<ResultScript> ().item = posiblesItems[idResult];
			itemObj.GetComponent<ResultScript> ().xp = blueprintdatabase.Blueprints[idblueprint[idResult]].XP;
			itemObj.GetComponent<ResultScript> ().ingredients = blueprintdatabase.Blueprints[idblueprint[idResult]].Ingredients;
			itemObj.transform.SetParent (GameObject.FindGameObjectWithTag("Finish").transform);
			itemObj.transform.position = GameObject.FindGameObjectWithTag("Finish").transform.position;
			itemObj.GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);
			itemObj.GetComponent<Image> ().sprite = posiblesItems[idResult].Sprite;
			itemObj.name = posiblesItems[idResult].Title;
		}

		
	}
}
