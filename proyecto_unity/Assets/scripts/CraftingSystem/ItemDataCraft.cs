﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class ItemDataCraft : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler,IPointerClickHandler {
	public Item item;
	private CraftSystem cra ;


	private Inventory inv;
	private Tooltip tooltip;


	void Start(){

		inv = GameObject.Find ("Inventory").GetComponent<Inventory> ();
		tooltip = inv.GetComponent<Tooltip> ();
		cra= FindObjectOfType<CraftSystem> ();

	}


	public void OnPointerEnter (PointerEventData eventData)
	{
		tooltip.Activate (item);

	}


	public void OnPointerExit (PointerEventData eventData)
	{
		tooltip.Deactivate ();
	}

	public void OnPointerClick (PointerEventData eventData){

		CraftSlot cc=	this.transform.parent.gameObject.GetComponent<CraftSlot>();
		cc.item = new Item ();
		Debug.Log(cc);
		tooltip.Deactivate ();
		cra.ListWithItem ();
		Destroy (this.gameObject);



	
	}





}
