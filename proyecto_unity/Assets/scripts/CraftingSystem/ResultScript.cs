﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;

public class ResultScript : MonoBehaviour ,IPointerEnterHandler,IPointerExitHandler,IPointerClickHandler{
	public Item item;
	private CraftSystem cra ;
	private Inventory inv;
	private Tooltip tooltip;
	public int xp;
	public List<int> ingredients =new List<int>();

	void Start(){

		inv = GameObject.Find ("Inventory").GetComponent<Inventory> ();
		tooltip = inv.GetComponent<Tooltip> ();
		cra= FindObjectOfType<CraftSystem> ();

	}


	public void OnPointerEnter (PointerEventData eventData)
	{
		tooltip.Activate (item);

	}


	public void OnPointerExit (PointerEventData eventData)
	{
		tooltip.Deactivate ();
	}

	public void OnPointerClick (PointerEventData eventData){
		
		LevelManager lvl = FindObjectOfType<LevelManager> ();
		if (xp <= lvl.XP) {
			lvl.XP -= xp;
			bool iscrafteable = true;
			int idIngrediente = 0;


			for (int i = 0; i <ingredients.Count; i++) {
				if (inv.CheckIfItemsIsInInventory (inv.database.FetchItemByID (ingredients [i]))) {
					inv.RemovItem (ingredients [i]);
				}else {
					iscrafteable = false;
					idIngrediente = i-1;
					break;
				}
			}

			if(!iscrafteable){
				for (int e = idIngrediente; e >= 0; e--) {
					if (inv.CheckIfItemsIsInInventory (inv.database.FetchItemByID (ingredients [e]))) {
						for (int i = 0; i < inv.items.Count; i++) {

							if (inv.items [i].ID == ingredients[e]) {
								ItemData data = inv.slots [i].transform.GetChild (0).GetComponent<ItemData> ();
								data.amount++;
								data.transform.GetChild (0).GetComponent<Text> ().text = data.amount.ToString ();
						//		Debug.Log (data.item.Title + "  " + data.amount + " " + data.transform.GetChild (0).GetComponent<Text> ().text);
								break;
							}
						}
					} else {
						inv.AddItem( ingredients [e]);
					//	Debug.Log( ingredients [e]);
					}

				}

				Debug.Log ("No hay ingredientes");
				this.gameObject.GetComponent<Image> ().color = Color.yellow;
				return;
			}



			inv.AddItem (item.ID);

			final ();
			return;

		} else {
			Debug.Log ("Xp insuficiente");
			this.gameObject.GetComponent<Image> ().color = Color.red;
		}




	}
	void final(){
		for (int i = 0; i < ingredients.Count; i++) {
			if (!inv.CheckIfItemsIsInInventory (inv.database.FetchItemByID (ingredients [i]))) {
				for (int e = 0; e < cra.slots.Count; e++) {

					if (cra.slots [e].item.ID != -1) {
						for (int w = 0; w < ingredients.Count; w++) {
							if (ingredients [w] == cra.slots [e].item.ID) {
								Destroy (cra.slots [e].gameObject.transform.GetChild (0).gameObject);
								cra.slots [e].item = new Item ();
							}
						}

					}

				}
				cra.ListWithItem ();
				Destroy (this.gameObject);
				return;			
			}

		}
	}



}
