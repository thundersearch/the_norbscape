﻿using UnityEngine;
using System.Collections;

public class Enemy2DFloor : MonoBehaviour {

	public float enemySpeed = 2;
	public Animator enemyAnimator;

	//facig
	public LayerMask enemyMask;
	Rigidbody2D myBody;
	Transform myTrans;
	float myWidth, myHeight;
	public float damagePoints= 1.0f;

	// Use this for initialization
	void Start () {
		myTrans = this.transform;
		myBody = this.GetComponent<Rigidbody2D>();
		SpriteRenderer mySprite = this.GetComponent<SpriteRenderer>();
		myWidth = mySprite.bounds.extents.x;
		myHeight = mySprite.bounds.extents.y;
	}
	

	void FixedUpdate () {
		//Vector2 lineCastPos = myTrans.position.toVector2() - myTrans.right.toVector2() * myWidth + Vector2.up * myHeight;
		Vector2 lineCastPos = myTrans.position - myTrans.right * myWidth;
		bool isGrounded = Physics2D.Linecast (lineCastPos, lineCastPos + Vector2.down, enemyMask);

		if (!isGrounded) {
			Vector3 currentRot = myTrans.eulerAngles;
			currentRot.y += 180;
			myTrans.eulerAngles = currentRot;
		}

		//Always move forward
		Vector2 myVel = myBody.velocity;
		myVel.x = myTrans.right.x * enemySpeed;
		myBody.velocity = myVel;
	}

	void OnTriggerStay2D(Collider2D Otro){
		if(Otro.tag == "Player"){
			Health health = Otro.gameObject.GetComponent<Health>();
			Ataque ataque= Otro.transform.Find("Ataque").gameObject.GetComponent<Ataque>();
			if (health== null ){			
				return;
			}
			if(health.healthPoints > 0){
				float rdano = (ataque.armor) / (5 + ataque.armor);
				float dano = (damagePoints * Time.deltaTime);
				float danoaplicado = dano - (dano * rdano);
				health.healthPoints -=danoaplicado;
			}
		}
	}




}
