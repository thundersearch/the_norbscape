﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour {

	private bool fullscreen;
	private int textureQuality;
	private int antialiasing;
	private int vSync;
	private int resolutionIndex;
	private float musicVolume;

    public Toggle fullscreenToggle;
    public Dropdown resolutionDropdown;
    public Dropdown textureQualityDropdown;
    public Dropdown antialiasingDropdown;
    public Dropdown vSyncDropdown;
    public Slider musicVolumeSlider;

	public AudioManager audioSource;
    public Resolution[] resolutions;

    void OnEnable()
    {

			fullscreenToggle.onValueChanged.AddListener(delegate {OnFullScreenToggle();});
		resolutionDropdown.onValueChanged.AddListener(delegate {OnResolutionChangue();});
		textureQualityDropdown.onValueChanged.AddListener(delegate {OnTextureQualityChangue();});
		antialiasingDropdown.onValueChanged.AddListener(delegate {OnAntialiasingChangue();});
		vSyncDropdown.onValueChanged.AddListener(delegate {OnVSyncChangue();});
		musicVolumeSlider.onValueChanged.AddListener(delegate {OnMusicVolumeChangue();});



        resolutions = Screen.resolutions;
		foreach (Resolution resolution in resolutions) {
			resolutionDropdown.options.Add(new Dropdown.OptionData(resolution.ToString()));
		}
    }

	public void OnFullScreenToggle(){
		fullscreen = Screen.fullScreen = fullscreenToggle.isOn;
	}
		
	public void OnResolutionChangue(){
		Screen.SetResolution (resolutions [resolutionDropdown.value].width, resolutions [resolutionDropdown.value].height, Screen.fullScreen);
	} 

	public void OnTextureQualityChangue(){
		QualitySettings.masterTextureLimit = textureQuality = textureQualityDropdown.value;

	}

	public void OnAntialiasingChangue(){
		QualitySettings.antiAliasing = antialiasing = (int)Mathf.Pow (2f, antialiasingDropdown.value);
	}

	public void OnVSyncChangue(){
		QualitySettings.vSyncCount = vSync = vSyncDropdown.value;
	}

	public void OnMusicVolumeChangue(){
		audioSource.BGM.volume = musicVolume = musicVolumeSlider.value;
	}

	public void SaveSettings(){
		
	}

	public void LoadSettings(){
		
	}

}
