﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {
	public GameObject deathParticulesPrefab = null;
	ItemDatabase database;
	LevelManager lvl;
	public LevelManager gameLevelManager;
	public bool destroyedOnDeath = false;
	public bool isplayer = false;
	public bool isBoos = false;
	public int loot;
	public int itemboos;
	int rnd;
	Item itm;
	Inventory inv ;

	public float healthPoints{
		get{ 
			return _healthPoints;
		}
		set {
			_healthPoints= value;
			if(_healthPoints <=0 ){
				
				if(deathParticulesPrefab != null ){
					GameObject obj = Instantiate(deathParticulesPrefab);
					obj.transform.position = this.transform.position;
					ItemScene item =  obj.GetComponent<ItemScene> ();
					if(isBoos){
						rnd = itemboos;
						item.Stack = 1;
						itm =database.FetchItemByID(rnd);
						lvl.XP += Random.Range (100, 300);
						lvl.money += Random.Range (500, 1000);
						FindObjectOfType<Animacion>().anim.SetBool("Play",true);						
					}else{
						do{
							rnd = Random.Range(20,150);
							itm =database.FetchItemByID(rnd);
						}while(loot != itm.Rarity);	
						item.Stack = Random.Range (1,3);
						lvl.XP += Random.Range (10, 30);
						lvl.money += Random.Range (10, 50);

					}

					item.id = rnd;
					item.gameObject.GetComponent<SpriteRenderer> ().sprite = itm.Sprite;
					if(item.id ==22){
						LevelManager lvl = FindObjectOfType<LevelManager> ();
						lvl.LoadLevel (5,Vector3.zero);
					}


					if (destroyedOnDeath) {
						_healthPoints = 0;
						Destroy (gameObject);
					}
				}
				if(isplayer){
					FigthBoos f = FindObjectOfType<FigthBoos> ();
					if(f != null){
						f.child.SetActive (false);
					}
					gameLevelManager.Respawn ();
				}				
			}
		}
	}
	// Use this for initialization
	void Start () {
		
		gameLevelManager = FindObjectOfType<LevelManager> ();
		inv = FindObjectOfType<Inventory> ();
		database = FindObjectOfType<ItemDatabase> ();
		lvl = FindObjectOfType<LevelManager> ();
		//healthPoints = 12;
	}
	
	// Update is called once per frame
	void Update () {
	


	}

	[SerializeField]
	private float _healthPoints;
}
