﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;



public class Inventory : MonoBehaviour {

	public string FileName;

	public bool posionActive =false;
	GameObject	inventoryPanel;
	GameObject	slotPanel;
	public ItemDatabase database;
	public GameObject Potion;
	public GameObject	inventorySlot;
	public GameObject	inventoryItem;
	pause pau;
	int slotAmount;
	public List<Item> items =new List<Item>();
	public List<GameObject>	slots =new List<GameObject>();
	private JsonData JsonInventory;
	public bool show =false;
	public bool showv =false;
	public GameObject panelventa;

	private LevelManager lvmanager;

	private static Inventory inv  = null;
	public static Inventory invInstance {
		get{
			if(inv ==null){
				GameObject questObject = new GameObject ("Defaul");
				inv = questObject.AddComponent<Inventory> ();
			}

			return inv;
		}


	}

	void Awake(){
		if(inv){
			DestroyImmediate (gameObject);
			return;
		}
		inv = this;
		DontDestroyOnLoad (inv);
	}


	void Start(){
		pau = FindObjectOfType<pause> ();
		lvmanager = FindObjectOfType<LevelManager> ();
		database = GetComponent<ItemDatabase>();
		panelventa = GameObject.Find ("PanelVenta");

		panelventa.SetActive (false);
		slotAmount = 91;
		inventoryPanel = GameObject.Find ("Inventory Panel");
		slotPanel = inventoryPanel.transform.FindChild ("Slot Panel").gameObject;
		for(int i=0; i<slotAmount; i++){
			items.Add (new Item ());
			slots.Add (Instantiate(inventorySlot));
			slots [i].GetComponent<Slot>().id=i;
			slots [i].transform.SetParent (slotPanel.transform);
			slots [i].GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);
			if(i==	80){
				
				i++;
				items.Add (new Item ());
				slots.Add (Instantiate(inventorySlot));
				slots [i].GetComponent<Slot>().id=i;
				slots [i].GetComponent<Slot>().type="head";
				slots [i].transform.SetParent (inventoryPanel.transform.FindChild ("Equipment Panel").gameObject.transform);
				slots [i].GetComponent<RectTransform>().anchoredPosition= new Vector3 (142,360,0);
				slots [i].GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);

				i++;
				items.Add (new Item ());
				slots.Add (Instantiate(inventorySlot));
				slots [i].GetComponent<Slot>().id=i;
				slots [i].GetComponent<Slot>().type="torso";
				slots [i].transform.SetParent (inventoryPanel.transform.FindChild ("Equipment Panel").gameObject.transform);
				slots [i].GetComponent<RectTransform>().anchoredPosition= new Vector3 (142,278,0);
				slots [i].GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);


				i++;
				items.Add (new Item ());
				slots.Add (Instantiate(inventorySlot));
				slots [i].GetComponent<Slot>().id=i;
				slots [i].GetComponent<Slot>().type="feet";
				slots [i].transform.SetParent (inventoryPanel.transform.FindChild ("Equipment Panel").gameObject.transform);
				slots [i].GetComponent<RectTransform>().anchoredPosition= new Vector3 (142,158,0);
				slots [i].GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);

				i++;
				items.Add (new Item ());
				slots.Add (Instantiate(inventorySlot));
				slots [i].GetComponent<Slot>().id=i;
				slots [i].GetComponent<Slot>().type="up";
				slots [i].transform.SetParent (inventoryPanel.transform.FindChild ("Equipment Panel").gameObject.transform);
				slots [i].GetComponent<RectTransform>().anchoredPosition= new Vector3 (54.30002f,113,0);
				slots [i].GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);

				i++;
				items.Add (new Item ());
				slots.Add (Instantiate(inventorySlot));
				slots [i].GetComponent<Slot>().id=i;
				slots [i].GetComponent<Slot>().type="up";
				slots [i].transform.SetParent (inventoryPanel.transform.FindChild ("Equipment Panel").gameObject.transform);
				slots [i].GetComponent<RectTransform>().anchoredPosition= new Vector3 (114.3f,82,0);
				slots [i].GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);

				i++;
				items.Add (new Item ());
				slots.Add (Instantiate(inventorySlot));
				slots [i].GetComponent<Slot>().id=i;
				slots [i].GetComponent<Slot>().type="up";
				slots [i].transform.SetParent (inventoryPanel.transform.FindChild ("Equipment Panel").gameObject.transform);
				slots [i].GetComponent<RectTransform>().anchoredPosition= new Vector3 (174.3f,82,0);
				slots [i].GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);

				i++;
				items.Add (new Item ());
				slots.Add (Instantiate(inventorySlot));
				slots [i].GetComponent<Slot>().id=i;
				slots [i].GetComponent<Slot>().type="up";
				slots [i].transform.SetParent (inventoryPanel.transform.FindChild ("Equipment Panel").gameObject.transform);
				slots [i].GetComponent<RectTransform>().anchoredPosition= new Vector3 (234.3f,113,0);
				slots [i].GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);

				i++;
				items.Add (new Item ());
				slots.Add (Instantiate(inventorySlot));
				slots [i].GetComponent<Slot>().id=i;
				slots [i].GetComponent<Slot>().type="hand";
				slots [i].transform.SetParent (inventoryPanel.transform.FindChild ("Equipment Panel").gameObject.transform);
				slots [i].GetComponent<RectTransform>().anchoredPosition= new Vector3 (54.29999f,300,0);
				slots [i].GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);

				i++;
				items.Add (new Item ());
				slots.Add (Instantiate(inventorySlot));
				slots [i].GetComponent<Slot>().id=i;
				slots [i].GetComponent<Slot>().type="hand";
				slots [i].transform.SetParent (inventoryPanel.transform.FindChild ("Equipment Panel").gameObject.transform);
				slots [i].GetComponent<RectTransform>().anchoredPosition= new Vector3 (234.3f,300,0);
				slots [i].GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);

				i++;
				items.Add (new Item ());
				slots.Add (Instantiate(inventorySlot));
				slots [i].GetComponent<Slot>().id=i;
				slots [i].GetComponent<Slot>().type="weapon";
				slots [i].transform.SetParent (inventoryPanel.transform.FindChild ("Equipment Panel").gameObject.transform);
				slots [i].GetComponent<RectTransform>().anchoredPosition= new Vector3 (278,218,0);
				slots [i].GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);
			}

		}




		//Debug.Log (items);
		//AddItemsForJsonFile ();
		//CreateFileJsonForInventory ();


	}
	void Update(){
		if((show&&Input.GetButtonDown("Pause"))||Input.GetKeyDown("e")&&lvmanager.isActivePlayer&&!showv && !lvmanager.isDialog&& !pau.paused){
			show = !show;	
			if (show) {
				Time.timeScale = 0;
			} else {
				Time.timeScale = 1;
			}
		}

		if(show){
			inventoryPanel.SetActive (true);


		}else if(lvmanager.isActivePlayer){
			inventoryPanel.SetActive (false);

		}else{
			inventoryPanel.SetActive (false);
		

		}

	}


	public void RemovItem(int id){
		for (int i = 0; i < items.Count; i++) {

			if (items [i].ID == id) {
				ItemData data = slots [i].transform.GetChild (0).GetComponent<ItemData> ();

				if (data.amount == 1) {
					items [i] = new Item ();
					Destroy(slots [i].transform.GetChild (0).gameObject);
				} else if(data.amount >1) {					
					data.amount--;
					if(data.amount == 1){
						data.transform.GetChild (0).GetComponent<Text> ().text = "";
					}else{
						data.transform.GetChild (0).GetComponent<Text> ().text = data.amount.ToString ();
					}
				}
				return;

			}
		}
	
	}
	public void AddItem(int id){
	

		Item itemToAdd = database.FetchItemByID (id);

		if (itemToAdd.Stackable && CheckIfItemsIsInInventory (itemToAdd)) {
			
			for (int i = 0; i < items.Count; i++) {
				
				if (items [i].ID == id) {
					ItemData data = slots [i].transform.GetChild (0).GetComponent<ItemData> ();
					data.amount++;
					data.transform.GetChild (0).GetComponent<Text> ().text = data.amount.ToString ();
					Debug.Log (data.item.Title +"  "+data.amount +" "+data.transform.GetChild (0).	GetComponent<Text> ().text);
					break;
				} 
			}


		} else {


			for (int i = 0; i < items.Count; i++) {
				if (items [i].ID == -1) {					
					items [i] = itemToAdd;
					GameObject itemObj = Instantiate (inventoryItem);
					itemObj.GetComponent<ItemData> ().item = itemToAdd;
					itemObj.GetComponent<ItemData> ().amount = 1;
					itemObj.GetComponent<ItemData> ().slot = i;

					itemObj.transform.SetParent (slots [i].transform);
					itemObj.transform.position = slots [i].transform.position;
					itemObj.GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);
					itemObj.GetComponent<Image> ().sprite = itemToAdd.Sprite;
					itemObj.name = itemToAdd.Title;
					Debug.Log (itemToAdd.Title);
					break;
				}
			
			}
		}
	}

	public bool CheckIfItemsIsInInventory(Item item){
	
		for (int i = 0; i < items.Count; i++) {
			if (items[i].ID == item.ID) {
				return true;
			}
		}
		
		return false;
	}



	public void AddItemsForJsonFile(){
		JsonInventory = JsonMapper.ToObject (File.ReadAllText(Application.dataPath +"/StreamingAssets/Inventory_"+FileName+".json"));

		for (int i =0;i<JsonInventory.Count;i++){
			int slot = (int)JsonInventory [i] ["slot"];
			int iditem = (int)JsonInventory [i] ["id"];
			int amount = (int)JsonInventory [i] ["amount"];
			if(iditem == -1){
				return;
			}
			Item itemToAdd = database.FetchItemByID (iditem);

			items [slot] = itemToAdd;
			GameObject itemObj = Instantiate (inventoryItem);
			itemObj.GetComponent<ItemData> ().item = itemToAdd;

			itemObj.GetComponent<ItemData> ().slot = slot;

			itemObj.transform.SetParent (slots [slot].transform);
			itemObj.transform.position = slots [slot].transform.position;
			itemObj.GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);
			itemObj.GetComponent<Image> ().sprite = itemToAdd.Sprite;
			itemObj.name = itemToAdd.Title;

			itemObj.GetComponent<ItemData> ().amount =amount;
			if(amount > 1){
			itemObj.transform.GetChild (0).GetComponent<Text> ().text = amount.ToString ();
			}
			if(slot > 80){
				if(iditem == 25 ){
					LevelManager.Theplayer.GetComponent<Controladormovimiento> ().dash = true;
				}
				if(iditem == 24 ){
					LevelManager.Theplayer.GetComponent<Controladormovimiento> ().velocidad = true;
				}
				if(iditem == 23){
					LevelManager.Theplayer.GetComponent<Controladormovimiento> ().dsalto = true;

				}
				if(iditem == 22){
					LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attackA=LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attack ;
					LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attack += LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attackA*.2f;

				}

				LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().armor += itemToAdd.Defence;
				LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attack += itemToAdd.Power;
			}
				

		}

	}


	public void CreateFileJsonForInventory(){
		int Cont = 0;
		string cadena = "[";
		for (int i =0;i<items.Count;i++){			
			if (items [i].ID != -1) {
				if (Cont == 0) {
					Cont++;
					cadena = cadena+"{ \"id\":"; 
					cadena = cadena + items [i].ID+ ", \"amount\":";
					ItemData data = slots [i].transform.GetChild (0).GetComponent<ItemData> ();
					cadena = cadena + data.amount+",\"slot\":"+i+"}";
				} else {
					cadena = cadena+",{ \"id\":"; 
					cadena = cadena + items [i].ID+ ", \"amount\":";
					ItemData data = slots [i].transform.GetChild (0).GetComponent<ItemData> ();
					cadena = cadena + data.amount+",\"slot\":"+i+"}";
				
				}

			} 
		}
		 cadena =cadena + "]";


		if (cadena == "[]") {
			System.IO.File.WriteAllText("Assets/StreamingAssets/Inventory_"+FileName+".json", "[{\"id\":-1 ,\"amount\":0 ,\"slot\":0}]");
		} else {
			System.IO.File.WriteAllText("Assets/StreamingAssets/Inventory_"+FileName+".json", cadena);
		}

		
	}

	public void CleanInventory(){
		
		for(int i=0;i<items.Count;i++){
			if (items [i].ID != -1) {
				Destroy(slots [i].transform.GetChild (0).gameObject);
				items[i] = new Item ();
			}

		}	

	}




}
