﻿using UnityEngine;

using System.Collections;
using UnityEngine.EventSystems;
using System;

public class ItemData : MonoBehaviour,IBeginDragHandler,IDragHandler,IEndDragHandler,IPointerEnterHandler,IPointerExitHandler,IPointerClickHandler{
	public Item item;
	public int amount;
	public int slot;



	private Inventory inv;
	private Tooltip tooltip;
	private Vector2 offset;

	void Start(){

		inv = GameObject.Find ("Inventory").GetComponent<Inventory> ();
		tooltip = inv.GetComponent<Tooltip> ();

	}


	public void OnPointerEnter (PointerEventData eventData)
	{
		tooltip.Activate (item);

	}


	public void OnPointerExit (PointerEventData eventData)
	{
		tooltip.Deactivate ();
	}


	public void OnBeginDrag (PointerEventData eventData)
	{	
		
		if(item != null){
			string typet = inv.slots [slot].GetComponent<Slot> ().type;
			if(typet == "hand"|| typet == "up" || typet == "weapon" || typet == "head"|| typet == "torso"|| typet == "legs"|| typet == "feet" ){
				Debug.Log ("ItemD ID:"+item.ID);
				if(item.ID== 25){
					LevelManager.Theplayer.GetComponent<Controladormovimiento> ().dash = false;
				}
				if(item.ID== 24){
					LevelManager.Theplayer.GetComponent<Controladormovimiento> ().velocidad = false;
				}
				if(item.ID== 23){
					LevelManager.Theplayer.GetComponent<Controladormovimiento> ().dsalto = false;
				}
				if(item.ID== 22){
					LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attack=LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attackA ;
				}
					
				LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().armor-= item.Defence;
				LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attack-= item.Power;
			}
			offset = eventData.position - new Vector2 (this.transform.position.x,this.transform.position.y);
			this.transform.SetParent (this.transform.parent.parent.parent);
			this.transform.position = eventData.position - offset;
			GetComponent<CanvasGroup> ().blocksRaycasts = false;
		}
	}


	public void OnDrag (PointerEventData eventData)
	{
		if(item != null){
			this.transform.position = eventData.position - offset;
		}
	}



	public void OnEndDrag (PointerEventData eventData)
	{	
		this.transform.SetParent (inv.slots[slot].transform);
		this.transform.position = inv.slots[slot].transform.position;
		GetComponent<CanvasGroup> ().blocksRaycasts = true;

		string typet = inv.slots [slot].GetComponent<Slot> ().type;
		if(typet == "hand"||typet == "up" || typet == "weapon" || typet == "head"|| typet == "torso"|| typet == "legs"|| typet == "feet"){
			Debug.Log ("ItemD ID:"+item.ID);
			if(item.ID== 25){
				LevelManager.Theplayer.GetComponent<Controladormovimiento> ().dash = true;
			}
			if(item.ID== 24){
				LevelManager.Theplayer.GetComponent<Controladormovimiento> ().velocidad = true;
			}
			if(item.ID== 23){
				LevelManager.Theplayer.GetComponent<Controladormovimiento> ().dsalto = true;
			}
			if(item.ID== 22){
				LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attackA=LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attack ;
				LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attack += LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attackA*.2f;

			}

			LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().armor+= item.Defence;
			LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attack+= item.Power;

		}

	}
	public void OnPointerClick(PointerEventData eventData){
		if (eventData.button == PointerEventData.InputButton.Right) {
			if(item.Type =="use" && !inv.posionActive){
				inv.posionActive = true;
				GameObject obj = Instantiate (inv.Potion);
				obj.GetComponent<SpriteRenderer>().sprite = item.Sprite;
				obj.GetComponent<ThrowObject> ().item = item;
				inv.RemovItem (item.ID);
				Debug.Log ("Right click");
			}
			if(item.Health != 0 ){
				
				LevelManager.Theplayer.GetComponent<Health> ().healthPoints+=item.Health;
				if(Corazon.vidaMax < LevelManager.Theplayer.GetComponent<Health> ().healthPoints){
					LevelManager.Theplayer.GetComponent<Health> ().healthPoints = Corazon.vidaMax;
				}
				inv.RemovItem (item.ID);
				Debug.Log ("Right click");
			}
		}
		
	}


}
