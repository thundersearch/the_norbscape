﻿	using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;	
using System;

public class Slot : MonoBehaviour,IDropHandler {
	public int id;
	public  string type;
	private Inventory inv;


	void Start(){
		inv = GameObject.Find ("Inventory").GetComponent<Inventory>();

	}



	public void OnDrop (PointerEventData eventData)
	{
		ItemData droppedItem = eventData.pointerDrag.GetComponent<ItemData> ();
		//Debug.Log ("Tipo del item " + droppedItem.item.Type);
		if(droppedItem.item.Type != type && type != ""){
			Debug.Log (1);
			return;
		}



		if (inv.items [id].ID == -1) {
			inv.items [droppedItem.slot] = new Item ();
			inv.items [id] = droppedItem.item;
			droppedItem.slot = id;			
		} else if(droppedItem.slot != id) {
			if(type == "hand"|| type == "up" || type == "weapon" || type == "head"|| type == "torso"|| type == "legs"|| type == "feet"){
				Debug.Log ("Slotactual   ID:"+inv.items[id].ID);
				if(inv.items[id].ID== 25){					
					LevelManager.Theplayer.GetComponent<Controladormovimiento> ().dash = false;
				}  
				if(inv.items[id].ID== 24){					
					LevelManager.Theplayer.GetComponent<Controladormovimiento> ().velocidad = false;
				}  
					
				if(inv.items[id].ID == 23){
					LevelManager.Theplayer.GetComponent<Controladormovimiento> ().dsalto = false;

				}
				if(inv.items[id].ID == 22){
					LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attack=LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attackA ;

				}
				LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().armor-= inv.items[id].Defence;
				LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attack-= inv.items[id].Power;

			}
			string typet = inv.slots [droppedItem.slot].GetComponent<Slot> ().type;
			if(type == "hand"||typet == "up" || typet == "weapon" || typet == "head"|| typet == "torso"|| typet == "legs"|| typet == "feet"){
				Debug.Log ("SlotNuevo ID:"+droppedItem.item.ID);
				if(inv.items[id].ID == 25 ){
					LevelManager.Theplayer.GetComponent<Controladormovimiento> ().dash = true;
				}
				if(inv.items[id].ID == 24 ){
					LevelManager.Theplayer.GetComponent<Controladormovimiento> ().velocidad = true;
				}
				if(inv.items[id].ID == 23){
					LevelManager.Theplayer.GetComponent<Controladormovimiento> ().dsalto = true;

				}
				if(inv.items[id].ID == 22){
					LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attackA=LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attack ;
					LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attack += LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attackA*.2f;

				}
				LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().armor+=inv.items[id].Defence;
				LevelManager.Theplayer.transform.FindChild("Ataque").GetComponent<Ataque> ().attack+= inv.items[id].Power;

			}

			Transform item = this.transform.GetChild (0);
			item.GetComponent<ItemData> ().slot = droppedItem.slot;
			item.transform.SetParent (inv.slots[droppedItem.slot].transform);
			item.transform.position = inv.slots [droppedItem.slot].transform.position;

			droppedItem.slot = id;
			droppedItem.transform.SetParent(this.transform);
			droppedItem.transform.position=this.transform.position;

			inv.items [droppedItem.slot] = item.GetComponent<ItemData> ().item;
			inv.items [id] = droppedItem.item;


		
		}

	}




}
