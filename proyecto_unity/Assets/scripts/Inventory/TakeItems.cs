﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TakeItems : MonoBehaviour {
	private Inventory inventory;
	private GameObject item;
	public GameObject ObjCorazon;
	public GameObject Ground;
	public LayerMask layer;
	public float radio;
	void Start () {
		
		inventory = GameObject.Find ("Inventory").GetComponent<Inventory>();

	}
	
	// Update is called once per frame
	void Update () {
		
		if(Physics2D.OverlapCircle (Ground.transform.position, radio, layer)){
			item = Physics2D.OverlapCircle (Ground.transform.position, radio, layer).gameObject;
			if(item.tag =="InventoryItem"){

				Debug.Log (item.GetComponent<ItemScene> ().Stack);
				for(int i=0 ; i<item.GetComponent<ItemScene> ().Stack ;i++){
					inventory.AddItem (item.GetComponent<ItemScene> ().id);
				}

			}else if(item.tag == "Health"){
				
				if (LevelManager.Theplayer.GetComponent<Health> ().healthPoints < Corazon.vidaMax) {
					LevelManager.Theplayer.GetComponent<Health> ().healthPoints += item.GetComponent<ItemScene> ().Stack;						
				} else {
					for(int i=0 ; i<item.GetComponent<ItemScene> ().Stack ;i++){
						inventory.AddItem (item.GetComponent<ItemScene> ().id);
					}				
				}


			}else if(item.tag == "NewCorazon"){
				GameObject obj = Instantiate (ObjCorazon);
				obj.GetComponent<Corazon> ().corazon = Corazon.contCorazon;
				obj.transform.SetParent (GameObject.Find("HealthPanel").transform);
				obj.GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);
				GameObject.Find ("HealthPanel").GetComponent<RectTransform> ().sizeDelta+= new Vector2(80,0);
			}
			Destroy (item);

		}
	
	}
}
