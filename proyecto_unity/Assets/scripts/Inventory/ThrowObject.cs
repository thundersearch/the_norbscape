﻿using UnityEngine;
using System.Collections;

public class ThrowObject : MonoBehaviour {
	public float speed = 1f;
	public Item item;
	public LayerMask layerCrash;
	public LayerMask layer;
	public float ForceThrow;
	public bool isCrash = false;
	public bool isThrow = false;
	Rigidbody2D m_Rigidbody2D;

	void Start(){
		transform.position=LevelManager.Theplayer.transform.position;
		m_Rigidbody2D = this.GetComponent<Rigidbody2D> ();
	}
	void Update (){
		Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
		Vector3 dir = Input.mousePosition - pos;
		float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;


		if (!isThrow) {
			transform.position=new Vector3(LevelManager.Theplayer.transform.position.x+0.40f,LevelManager.Theplayer.transform.position.y+0.30f,-4);

			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		} else {
			isCrash=Physics2D.OverlapCircle(this.transform.position,.5f,layer);	

		}

		if(isCrash){
			FindObjectOfType<Inventory> ().posionActive = false;
			Collider2D[] hitColliders = Physics2D.OverlapCircleAll(this.transform.position,1.3f,layerCrash);
			for (int i = 0; i < hitColliders.Length; i++) {
				if(hitColliders[i].GetType() == typeof(CircleCollider2D) ){
					if (item.ID == 119) {
						hitColliders [i].gameObject.GetComponent<Debuf> ().SelectDebuf ("posion", 15);
						hitColliders [i].gameObject.GetComponent<Debuf> ().SelectDebuf ("slow", 10);
					}
					if (item.ID == 125) {
						hitColliders [i].gameObject.GetComponent<Debuf> ().SelectDebuf ("weakness", 30);
					}
				}

			}


			Destroy (this.gameObject);
		}

		if(Input.GetMouseButtonDown(0) && !isThrow){

			Vector3 direc = Quaternion.AngleAxis(angle, Vector3.forward) * Vector3.right;
			m_Rigidbody2D.AddForceAtPosition(direc*ForceThrow,pos);
			isThrow = true;
		}

	}
}
