﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour {
	private Item item;
	private string data;
	private GameObject tooltip;

	void Start(){
		tooltip = GameObject.Find ("Tooltip");
		tooltip.SetActive (false);

	}
	void Update(){

		if(tooltip.activeSelf){
			tooltip.transform.position = Input.mousePosition;
		}
	}
	public void Activate(Item item){
		this.item = item;
		ContructDataString ();
		tooltip.SetActive (true);
	}

	public void Deactivate(){
		tooltip.SetActive (false);
	}

	public void ContructDataString(){
		data = "<color=#000000><b>" + item.Title + "</b></color>";
		if(item.Description != ""){
			data += "\n" + item.Description; 
		}
		if(item.Power!= 0){
			data+="\nPower: " + item.Power;
		}
		if(item.Defence!= 0){
			data+="\nDefensa: " + item.Defence;
		}			
		if(item.Health!= 0){
			data+="\nCuracion: " + item.Health;
		}

			
		tooltip.transform.GetChild (0).GetComponent<Text> ().text = data;

	}
}
