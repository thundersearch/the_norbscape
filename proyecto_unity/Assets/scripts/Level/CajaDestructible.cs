﻿using UnityEngine;
using System.Collections;

public class CajaDestructible : MonoBehaviour {
	ItemDatabase database ;
	LevelManager lvl;
	public GameObject ItemEsena;
	public int nitems;
	public int loot;
	public bool espesificitem;
	Item item;
	int rnd ;

	// Use this for initialization
	void Start () {
		database = FindObjectOfType<ItemDatabase> ();
		lvl = FindObjectOfType<LevelManager> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public  void SoltarItems(){
		if (!espesificitem) {
			nitems = Random.Range (2, 4);

			for (int i = 0; i < nitems; i++) {
				loot = Random.Range (1, 3); 
				do {
					rnd = Random.Range (39, 112);	
					item = database.FetchItemByID (rnd);
				} while(loot != item.Rarity);

				GameObject obj = Instantiate (ItemEsena);
				obj.transform.position = this.transform.position;
				obj.GetComponent<ItemScene> ().id = rnd;
				obj.GetComponent<ItemScene> ().Stack = 1;
				obj.GetComponent<SpriteRenderer> ().sprite = item.Sprite;
			}

		} else {
		
			for (int i = 0; i < nitems; i++) {	
				item = database.FetchItemByID (loot);
				GameObject obj = Instantiate (ItemEsena);
				obj.transform.position = this.transform.position;
				obj.GetComponent<ItemScene> ().id = loot;
				obj.GetComponent<ItemScene> ().Stack = Random.Range(1,3);
				obj.GetComponent<SpriteRenderer> ().sprite = item.Sprite;
			}
		
		}

		lvl.XP += Random.Range (10, 30);
		lvl.money += Random.Range (10, 50);
		Destroy (this.gameObject);
	}
}
