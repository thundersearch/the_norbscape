﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
	public bool isActivePlayer = false;
	public GameObject panelHeart;
	public GameObject Menu;
	public GameObject Xpbar;
	public bool ongui = false;
	public bool isDialog =false;


	public float respawnDelay;
	public Controladormovimiento gamePlayer;
	public int levelActual;
	// Use this for initialization
	private static LevelManager level  = null;
	public static GameObject Theplayer = null; 




	public int XP{
		set{ 

			_XP = value;
			int curlvl =(int)(0.1f*Mathf.Sqrt(_XP));

			if(curlvl != currentLevel){
				currentLevel = curlvl;
				if (currentLevel == 0) {
					Xpbar.transform.FindChild ("TextLevel").gameObject.GetComponent<Text> ().text = "";
				} else {
					Xpbar.transform.FindChild ("TextLevel").gameObject.GetComponent<Text> ().text = currentLevel.ToString();
				}
			}

			int xpNextlvl = 100 * (currentLevel+1) * (currentLevel +1 );
			int differencexp = xpNextlvl - XP;
			int totalDifference = xpNextlvl - (100 * currentLevel * currentLevel);
			Xpbar.transform.FindChild ("TextXp").gameObject.GetComponent<Text> ().text =_XP.ToString();


			float porc = 1- ((float)differencexp / (float)totalDifference);
			Xpbar.transform.FindChild("Mask").gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 ((326 *porc)+1,80.5f);

		}
		get{
			return _XP;
		}		
	}
	public int currentLevel{
		set{ 
			_currentLevel = value;
		}
		get{
			return _currentLevel;
		}	
	}
	public int money{
		set{ 
			_money = value;
		}
		get{
			return _money;
		}	
	}
	public static LevelManager levelInstance {
		get{
			if(level ==null){
				GameObject questObject = new GameObject ("Defaul");
				level = questObject.AddComponent<LevelManager> ();
			}

			return level;
		}


	}

	void Awake(){
		if(level){
			DestroyImmediate (gameObject);
			return;
		}
		level = this;
		DontDestroyOnLoad (level);
	}

	void Start () {
		gamePlayer = FindObjectOfType<Controladormovimiento> ();
		panelHeart = GameObject.FindGameObjectWithTag ("HealthPanel");

	}

	// Update is called once per frame
	void Update () {
		if(isActivePlayer){
			gamePlayer.gameObject.SetActive (true);
			panelHeart.SetActive (true);
			Xpbar.SetActive (true);
			Menu.SetActive (false);
		}else if(levelActual == 5){
			gamePlayer.gameObject.SetActive (false);
			panelHeart.SetActive (false);
			Xpbar.SetActive (false);
			Menu.SetActive (false);

		}else{
			gamePlayer.gameObject.SetActive (false);
			panelHeart.SetActive (false);
			Xpbar.SetActive (false);


		}

	}


	/*void OnGUI(){
		GUIStyle myStyle = new GUIStyle ();
		myStyle.fontSize = 24;
		GUI.contentColor = Color.blue;
		if(ongui && (currentLevel !=0))
			GUI.Label (new Rect (Screen.width / 4, Screen.height / 2, 200f, 200f), "Felicidades subiste a nivel " + currentLevel, myStyle);
	}*/



	public void LoadLevel(int level ,Vector3 ubicacion){
		levelActual = level;
		Initiate.Fade ("nivel" +level, Color.black, 0.5f);
		StartCoroutine(PostLevelLoad ("nivel" +level,ubicacion));
	}

	public void Respawn(){
		isActivePlayer = false;
		StartCoroutine(RespawnCoroutine());

	}	

	public void setActivePlayerFalse(){
		isActivePlayer = false;
	}
	
	IEnumerator PostLevelLoad(string level ,Vector3 ubicacion){		
		yield  return new WaitForSeconds (2.5f);
		if (level == "nivel0") {
			Menu.SetActive (true);
			isActivePlayer = false;
		} else {
			isActivePlayer = true;
		}

		if( level == "nivel5"){
			isActivePlayer = false;
			Menu.SetActive (false);
		}

		if( level == "nivel131"){
			isActivePlayer = false;
			Menu.SetActive (false);
		}
		gamePlayer.gameObject.transform.position=ubicacion;
		gamePlayer.respawnPoint = ubicacion;



	}

	IEnumerator RespawnCoroutine(){
		
		yield  return new WaitForSeconds (respawnDelay);
		gamePlayer.gameObject.transform.position = gamePlayer.respawnPoint;
		isActivePlayer = true;
		gamePlayer.gameObject.GetComponent<Health>().healthPoints = 12;




	}
	[SerializeField]
	private int _XP;
	[SerializeField]
	private int _money;
	[SerializeField]
	private int _currentLevel;

}
