﻿using UnityEngine;
using System.Collections;

public class NextLevel : MonoBehaviour {
	public int Tolevel;
	public Vector3 Toposicion;
	public LevelManager gameLevelManager;

	private void Awake(){
		gameLevelManager = FindObjectOfType<LevelManager> ();

	}
	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			gameLevelManager.LoadLevel (Tolevel, Toposicion);
		}
	}
 
}
