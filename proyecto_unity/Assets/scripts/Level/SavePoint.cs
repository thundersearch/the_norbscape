﻿using UnityEngine;
using System.Collections;

public class SavePoint : MonoBehaviour {
	LevelManager lvManager;
	statusPlayer status;
	Inventory inv;

	void Start () {
		
		status = FindObjectOfType<statusPlayer> ();
		inv = FindObjectOfType<Inventory> ();
		lvManager = FindObjectOfType<LevelManager> ();
	}
	void OnTriggerEnter2D(Collider2D Otro){
		if(Otro.tag == "Player"){
			inv.CreateFileJsonForInventory ();
			lvManager.gamePlayer.respawnPoint = new Vector3(this.transform.position.x,this.transform.position.y,-5);
			status.saveFile ((int)this.transform.position.x,(int)this.transform.position.y,-5);
			Debug.Log ("Archivo salvado");
		}
	}
}
