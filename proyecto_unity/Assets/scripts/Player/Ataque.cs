﻿using UnityEngine;
using System.Collections;

public class Ataque : MonoBehaviour {
	public LayerMask Enemy_layer;
	Inventory inv;
	Animator anim;
	int count =0 ;

	public float radio=0;
	public float armor= 0;
	public float attack = 0;
	public float attackA=0;
	public float armorA=0;

	private void Awake(){
		inv = FindObjectOfType<Inventory> ();
		anim = LevelManager.Theplayer.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)){
			anim.SetBool ("ataque",true);
			StartCoroutine (VelocidadAtaque(0.2f));
		
				
		}
	
	}

	IEnumerator VelocidadAtaque(float delay){
		yield  return new WaitForSeconds (delay);
		anim.SetBool ("ataque",false);
	}
	void OnTriggerEnter2D(Collider2D other){	
		if(other.tag == "Enemy"){
			count++;
			Rigidbody2D rigi =other.transform.GetComponentInParent<Rigidbody2D>(); 
			Health health = other.transform.GetComponentInParent<Health>();
			enemy enemigo = other.transform.GetComponentInParent<enemy>();
			Debuf deb = other.transform.GetComponentInParent<Debuf> ();
			if (health== null ){
				return;
			}
			if(health.healthPoints > 0){
				rigi.AddForceAtPosition (new Vector2(1,1),this.transform.position);
				if (inv.items [90].ID == 188) {
					deb.SelectDebuf ("slow",6);	
				} 
				if (inv.items [90].ID == 186 && Enemy_layer != LayerMask.NameToLayer("Jefe")) {
					deb.SelectDebuf ("weakness",6);		

				} 
				float rdano = (enemigo.armor) / (5 + enemigo.armor);
			
				float danoaplicado = attack - (attack * rdano);
				health.healthPoints -=danoaplicado;

			}
		}

		if (other.tag == "Loot") {		

			CajaDestructible caja = other.gameObject.GetComponent<CajaDestructible> ();
			caja.SoltarItems ();

		}
	}
	void OnTriggerExit2D(Collider2D other){
		if(other.tag == "Enemy"){			
			count = 0;
		}
	}
}
