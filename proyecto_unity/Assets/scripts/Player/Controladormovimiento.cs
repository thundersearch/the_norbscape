﻿using UnityEngine;
using System.Collections;

public class Controladormovimiento : MonoBehaviour {

	//public bool isActive= true;
	public float maxSpeedt = 0;
	public float maxSpeed = 7f;
	bool facingRight = true; //posicion de la cara
	public float jumpForce = 650f; //fuerza de salto

	//Movimientos especiales
	public bool dsalto = false;
	private int consalto =0;
	public bool dash = false;

	public bool velocidad = false;

	//dash
	public DashState dashState;
	public float dashTimer;
	public float maxDash = 20f;
	public float dashForce =10f;
	public Vector2 savedVelocity;


	// detector de tierra 
	public Transform Ground_check ;
	public LayerMask ground_layers;
	private bool grounded;
	public float Radio_groud =0.1f;



	//Componenetes
	private Animator m_Anim;   
	private Rigidbody2D m_Rigidbody2D;
	private Transform m_CeilingCheck;
	public Vector3 respawnPoint;
	 
	public LevelManager gameLevelManager;
	private GameObject panelHeart; 

	private static Controladormovimiento control = null;
	public static Controladormovimiento controlInstance {
		get{
			if(control ==null){
				GameObject questObject = new GameObject ("Defaul");
				control = questObject.AddComponent<Controladormovimiento> ();
			}

			return control;
		}


	}


		

	private void Awake(){
		if(control){
			DestroyImmediate (gameObject);
			return;
		}
		control = this;
		DontDestroyOnLoad (control);		

		LevelManager.Theplayer = gameObject;

		m_Anim = GetComponent<Animator>();
		m_Rigidbody2D = GetComponent<Rigidbody2D>();
		gameLevelManager = FindObjectOfType<LevelManager> ();

	}


	void FixedUpdate () {		

		//para que salte
		grounded=Physics2D.OverlapCircle(Ground_check.position,Radio_groud,ground_layers);

		//para que se mueva
		if( dashState != DashState.Dashing){
			float move = Input.GetAxis ("Horizontal");
			float movey = Input.GetAxis ("Vertical");

			if (movey < 0) {
				m_Anim.SetBool ("agacharse", true);
			} else {
				m_Anim.SetBool ("agacharse", false);
			}
			if (velocidad) {
				m_Rigidbody2D.velocity = new Vector2 ((move * maxSpeed)+(move * maxSpeed)*.15f, m_Rigidbody2D.velocity.y);
			} else {
				m_Rigidbody2D.velocity = new Vector2 (move * maxSpeed, m_Rigidbody2D.velocity.y);
			}
			if (move > 0 &&!facingRight){
				Flip();
			}
			if (move < 0 && facingRight){
				Flip();
			}
		}

		m_Anim.SetBool("IsGrounded", grounded);

		// Set the vertical animation
		//m_Anim.SetFloat("vely", m_Rigidbody2D.velocity.y);
		if (m_Rigidbody2D.velocity.x > 0) {
			m_Anim.SetFloat ("velx", m_Rigidbody2D.velocity.x);
		} else {
		
			m_Anim.SetFloat ("velx", -1*m_Rigidbody2D.velocity.x);
		}

	}

	//voltea la cara al lugar del movimiento
	void Flip(){
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void Update () {
		

		//-------------------------------------------------------//
		if (dsalto) {
			if (consalto == 0 && Input.GetKeyDown (KeyCode.Space)) {
				m_Rigidbody2D.AddForce (new Vector2 (0, jumpForce));
				consalto++;

			}else if(consalto == 1 && Input.GetKeyDown (KeyCode.Space)){
				m_Rigidbody2D.AddForce (new Vector2 (0, jumpForce*0.5f));
				consalto++;
			}


			if (grounded) {
				consalto = 0;

			}
		} else {
			if (grounded && Input.GetKeyDown (KeyCode.Space)) {
				m_Rigidbody2D.AddForce (new Vector2 (0, jumpForce));
			}
		}


		//-------------------------------------------------------//

		if (dash) {
			switch (dashState){
			case DashState.Ready:
				m_Anim.SetBool("dash", false);
				var isDashKeyDown = Input.GetKeyDown (KeyCode.LeftShift);
				if(isDashKeyDown){
					m_Anim.SetBool("dash", true);
					if (facingRight) {
						m_Rigidbody2D.velocity =  new Vector2(dashForce, m_Rigidbody2D.velocity.y);
					} else {
						m_Rigidbody2D.velocity =  new Vector2(dashForce *  -1 , m_Rigidbody2D.velocity.y);
						
					}

					savedVelocity = m_Rigidbody2D.velocity;
					dashState = DashState.Dashing;
				}
				break;
			case DashState.Dashing:				
				dashTimer += Time.deltaTime * 3;
				if(dashTimer >= maxDash){
					dashTimer = maxDash;
					m_Rigidbody2D.velocity = savedVelocity;
					dashState = DashState.Cooldown;

				}
				break;
			case DashState.Cooldown:
				m_Anim.SetBool("dash", false);
				dashTimer -= Time.deltaTime;
				if(dashTimer <= 0){
					dashTimer = 0;
					dashState = DashState.Ready;
				}
				break;
			}

		}


	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "FallDetector") {
			gameLevelManager.Respawn();
		}
		if (other.tag == "Checkpoint") {
			respawnPoint = other.transform.position;
		}
	}





	public enum DashState 
	{
		Ready,
		Dashing,
		Cooldown
	}
}