﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class Corazon : MonoBehaviour {

	public static int vidaMax;
	public static int contCorazon;
	public static Transform theLastPosision;
	public int corazon=0;
	private Image vidat;
	public Sprite vida0, vida1, vida2, vida3, vida4;
	private Health vida;
	void Awake(){
		vidat = GetComponent<Image>();
		vidaMax += 4;
		contCorazon++;


	}

	void FixedUpdate(){
		if(corazon == contCorazon){
			theLastPosision = this.transform;

		}


		if (LevelManager.Theplayer != null) {
			vida = LevelManager.Theplayer.GetComponent<Health> ();

			if (vida.healthPoints <= corazon * 4 - 4) {
				vidat.sprite = vida0;
			} else if (vida.healthPoints <= corazon * 4 - 3 && vida.healthPoints >= corazon * 4 - 4) {
				vidat.sprite = vida1;
			} else if (vida.healthPoints <= corazon * 4 - 2 && vida.healthPoints >= corazon * 4 - 3) {
				vidat.sprite = vida2;
			} else if (vida.healthPoints <= corazon * 4 - 1 && vida.healthPoints >= corazon * 4 - 2) {
				vidat.sprite = vida3;
			} else if (vida.healthPoints >= corazon * 4) {
				vidat.sprite = vida4;
			}
		} else {
			vidat.sprite = vida0;
		}

}


}
