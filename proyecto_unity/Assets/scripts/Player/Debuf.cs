﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Debuf : MonoBehaviour {
	public bool isPlayer = false;
	public bool Posion = false;
	public bool Weakness= false;
	public bool slow = false;



	Health healt ;
	Ataque ataque;
	enemy enemigo;

	// Use this for initialization
	void Start () {
		healt = this.gameObject.GetComponent<Health> ();
		if(isPlayer){
			ataque = this.transform.FindChild("Ataque").GetComponent<Ataque>();
		}else{
			enemigo = this.gameObject.GetComponent<enemy>();
		}
	}

	// Update is called once per frame
	void Update () {
		if(Posion){
			StartCoroutine (Aplicationdebuff("posion"));
		}

	}

	public void SelectDebuf(string debuf,float seconds){


		if(isPlayer){
			if(debuf == "posion"){
				this.gameObject.GetComponent<SpriteRenderer> ().color = Color.green;
				Posion = true;
			}else if(debuf == "weakness"){
				this.gameObject.GetComponent<SpriteRenderer> ().color = Color.gray;
				ataque.armorA = ataque.armor;
				ataque.armor = ataque.armor * .5f;
				Weakness = true;
			}else if(debuf == "slow"){
				this.gameObject.GetComponent<SpriteRenderer> ().color = Color.blue;
				LevelManager.Theplayer.GetComponent<Controladormovimiento> ().maxSpeedt =LevelManager.Theplayer.GetComponent<Controladormovimiento> ().maxSpeed ;
				LevelManager.Theplayer.GetComponent<Controladormovimiento> ().maxSpeed = LevelManager.Theplayer.GetComponent<Controladormovimiento> ().maxSpeed * .45f;
				slow = true;
			}


		}else{
			if(debuf == "posion"){
				this.gameObject.GetComponent<SpriteRenderer> ().color = Color.green;
				Posion = true;
			}else if(debuf == "weakness"){
				this.gameObject.GetComponent<SpriteRenderer> ().color = Color.gray;
				enemigo.armortemp = enemigo.armor;
				enemigo.armor = enemigo.armor * .5f;
				Weakness = true;
			}else if(debuf == "slow"){
				this.gameObject.GetComponent<SpriteRenderer> ().color = Color.blue;
				enemigo.maxSpeedt = enemigo.maxSpeed ;
				enemigo.maxSpeed = enemigo.maxSpeed * .30f;
				slow = true;
			}

		}

		StartCoroutine (SelectDebufContainer(debuf,seconds));
	}
	IEnumerator SelectDebufContainer(string debuf,float seconds){		
		yield  return new WaitForSeconds (seconds);
		this.gameObject.GetComponent<SpriteRenderer> ().color = Color.white;
		if(isPlayer){
			if(debuf == "posion"){
				Posion = false;
			}else if(debuf == "weakness"){
				ataque.armor = ataque.armorA ;
				Weakness = false;
			}else if(debuf == "slow"){
				LevelManager.Theplayer.GetComponent<Controladormovimiento> ().maxSpeed =LevelManager.Theplayer.GetComponent<Controladormovimiento> ().maxSpeedt ;
				slow = false;
			}

		}else{
			if(debuf == "posion"){
				Posion = false;
			}else if(debuf == "weakness"){
				enemigo.armor = enemigo.armortemp;
				Weakness = false;
			}else if(debuf == "slow"){
				enemigo.maxSpeed = enemigo.maxSpeedt;
				slow = false;
			}

		}



	}
	IEnumerator Aplicationdebuff(string debuf){
		yield  return new WaitForSeconds (3);
		if(debuf == "posion"){
			if(isPlayer){
				float rdano = (ataque.armor) / (5 + ataque.armor);
				float danoaplicado = 3 - (3 * rdano);
				if(healt.healthPoints > 1 && (healt.healthPoints-danoaplicado)>0 ){
					healt.healthPoints -=danoaplicado;
				}

			}else{
				float rdano = (enemigo.armor) / (5 + enemigo.armor);
				float danoaplicado = 3 - (3 * rdano);
				healt.healthPoints -=danoaplicado;
			}
		}

	
	}
}
