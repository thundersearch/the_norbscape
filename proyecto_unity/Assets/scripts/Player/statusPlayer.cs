﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using LitJson;
using System.IO;
public class statusPlayer : MonoBehaviour {

	public LevelManager levelmanager;
	public Inventory inventory;
    public string[] perfil;
	public Text[] Player;
	public Button[] btnFile;
	public InputField Name;
	public JsonData[] jsonProfile;
	private JsonData  jsonBlanc;
    public dataPlayer blanc = new dataPlayer("", 1,0,0,0,0,0,0,0,0);
	private AnimatorMenu anima;
	private int tempid;
	private bool accEliminar = false;
	private bool accReiniciar = false;
	private bool accCambiarN = false;
	private bool Lname = false;

	// Use this for initialization
	void Awake(){
		levelmanager = FindObjectOfType<LevelManager> ();
		inventory = FindObjectOfType<Inventory> ();
		anima = FindObjectOfType<AnimatorMenu> ();


	}
	void Start(){ 
		jsonProfile= new JsonData[3];
		getData(0);
		getData(1);
        getData(2);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void reiniciar(int id){
		dataPlayer blanc1 = new dataPlayer(jsonProfile[id]["nombre"].ToString(), 131,15,-7,-5,0,0,0,12,3);
        jsonBlanc = JsonMapper.ToJson(blanc1);
		File.WriteAllText (Application.dataPath + "/StreamingAssets/Inventory_statsPlayer"+(id+1)+".json","[{\"id\":-1 ,\"amount\":0 ,\"slot\":0}]");
		File.WriteAllText((Application.dataPath + "/StreamingAssets/statsPlayer"+(id+1)+".json"), jsonBlanc.ToString());
		getData (id);
    }
	public void eliminar(int id){
        jsonBlanc = JsonMapper.ToJson(blanc);
		File.WriteAllText ((Application.dataPath + "/StreamingAssets/Inventory_statsPlayer"+(id+1)+".json"),"[{\"id\":-1 ,\"amount\":0 ,\"slot\":0}]");
		File.WriteAllText((Application.dataPath + "/StreamingAssets/statsPlayer"+(id+1)+".json"), jsonBlanc.ToString());
		getData (id);
    }
	public void cambiarNombre(int id){
		if (jsonProfile [id] ["nombre"].ToString () != "") {

			anima.showName (true);
			tempid = id;
		} else {
			anima.showName (false);
			Lname = false;
		}
	}
	public void Nuevo(){
		
		if (Name.text == ""){
			anima.showName (false);
			Lname = false;
		}else if(Lname == true){
			

			Lname = false;
			dataPlayer Save = new dataPlayer(Name.text,
				(int)jsonProfile[tempid]["nivel"],
				(int)jsonProfile[tempid]["x"],
				(int)jsonProfile[tempid]["y"],
				(int)jsonProfile[tempid]["z"],
				(int)jsonProfile[tempid]["money"],
				(int)jsonProfile[tempid]["xp"],
				(int)jsonProfile[tempid]["currentLevel"],
				(int)jsonProfile[tempid]["health"],
				(int)jsonProfile[tempid]["heart"]);
			
			JsonData JSave = JsonMapper.ToJson(Save);
			File.WriteAllText((Application.dataPath + "/StreamingAssets/statsPlayer"+(tempid+1)+".json"), JSave.ToString());
			getData (tempid);
			Name.text = "";
			anima.showName (false);
		}else{
			anima.showName (false);
			anima.showNew (false);
			dataPlayer nP = new dataPlayer(Name.text,131,15,-7,-5,0,0,0,12,3);
			jsonBlanc = JsonMapper.ToJson(nP);
			File.WriteAllText ((Application.dataPath + "/StreamingAssets/Inventory_statsPlayer"+(tempid+1)+".json"),"[{\"id\":-1 ,\"amount\":0 ,\"slot\":0}]");
			File.WriteAllText((Application.dataPath + "/StreamingAssets/statsPlayer"+(tempid+1)+".json"), jsonBlanc.ToString());
			Name.text = "";
			getData(tempid);
			inventory.CleanInventory();
			inventory.FileName="statsPlayer"+(tempid+1);

			levelmanager.isActivePlayer = true;
			inventory.AddItemsForJsonFile ();
			StarHeart ((int)jsonProfile[tempid]["heart"]);
			levelmanager.gamePlayer.gameObject.GetComponent<Health> ().healthPoints = (int)jsonProfile [tempid] ["health"];
			levelmanager.isActivePlayer = false;

			levelmanager.XP =(int)jsonProfile[tempid]["xp"];
			levelmanager.currentLevel =(int)jsonProfile[tempid]["currentLevel"];
			levelmanager.money= (int)jsonProfile[tempid]["money"];


			levelmanager.LoadLevel ((int)jsonProfile [tempid] ["nivel"], new Vector3 ((int)jsonProfile [tempid] ["x"], (int)jsonProfile [tempid] ["y"], (int)jsonProfile [tempid] ["z"]));
		}
	}
	public void jugar(int id){

		if (accEliminar) {
			eliminar (id);
			accDes (1);
		} else if (accReiniciar) {
			reiniciar (id);
			accDes (2);
		} else if (accCambiarN) {
			Lname = true;
			cambiarNombre (id);
			accDes (3);
		
		}else{
			if (anima.isShowL) {
				if (jsonProfile [id] ["nombre"].ToString () != "") {
					anima.showLoad (false);
					inventory.FileName="statsPlayer"+(id+1);
					inventory.CleanInventory();

					levelmanager.isActivePlayer = true;
					inventory.AddItemsForJsonFile ();
					StarHeart ((int)jsonProfile[id]["heart"]);
					levelmanager.gamePlayer.gameObject.GetComponent<Health> ().healthPoints = (int)jsonProfile [id] ["health"];
					levelmanager.isActivePlayer = false;

					levelmanager.XP =(int)jsonProfile[id]["xp"];
					levelmanager.currentLevel =(int)jsonProfile[id]["currentLevel"];
					levelmanager.money= (int)jsonProfile[id]["money"];

					tempid = id;
					levelmanager.LoadLevel ((int)jsonProfile [id] ["nivel"], new Vector3 ((int)jsonProfile [id] ["x"], (int)jsonProfile [id] ["y"], (int)jsonProfile [id] ["z"]));

				} else {
					Debug.Log ("Juego no existe ");

				}
			} else {
				if (jsonProfile [id] ["nombre"].ToString () != "") {
					Debug.Log ("Ya existe");
				} else {
					tempid = id;
					anima.showName (true);
				}
			
			} 
		}

    }
    
	public void accDes(int acc){
		if(acc==1 && accReiniciar != true && accCambiarN != true){
			accEliminar = !accEliminar;
			if (accEliminar) {
				btnFile [0].image.color = Color.red; 
				btnFile [1].image.color = Color.red;
				btnFile [2].image.color = Color.red;
			} else {
				btnFile [0].image.color = Color.white;
				btnFile [1].image.color = Color.white;
				btnFile [2].image.color = Color.white;
			}
		}else if(acc==2 && accEliminar!= true && accCambiarN != true){
			accReiniciar=!accReiniciar;

			if (accReiniciar) {
				btnFile [0].image.color = Color.blue;
				btnFile [1].image.color = Color.blue;
				btnFile [2].image.color = Color.blue;
			} else {
				btnFile [0].image.color = Color.white;
				btnFile [1].image.color = Color.white;
				btnFile [2].image.color = Color.white;
			}

		}else if(acc==3 && accEliminar!= true && accReiniciar != true){
			accCambiarN=!accCambiarN;
			 
			if (accCambiarN) {
				btnFile [0].image.color = Color.green;
				btnFile [1].image.color = Color.green;
				btnFile [2].image.color = Color.green;
			} else {
				btnFile [0].image.color = Color.white;
				btnFile [1].image.color = Color.white;
				btnFile [2].image.color = Color.white;
			}

		}


	
	}
	private void getData(int id){

		jsonProfile[id] = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/statsPlayer"+(id+1)+".json"));
        if (jsonProfile[id]["nombre"].ToString() == "")
        {
			Player[id].text = "empty";
          
        }
        else
        {
			string level;
			if ((int)jsonProfile [id] ["nivel"] == 131) {
				level ="1";
			} else {
				level = jsonProfile [id] ["nivel"].ToString ();
			}
			Player[id].text = "Nombre del jugador: " + jsonProfile[id]["nombre"].ToString() + "\n Nivel: " + level;
        }
    }

	public void saveFile(int x, int y ,int z  ){
		Health h = levelmanager.gamePlayer.gameObject.GetComponent<Health> ();
		dataPlayer Save = new dataPlayer(jsonProfile[tempid]["nombre"].ToString() ,levelmanager.levelActual,x,y,z,levelmanager.money,levelmanager.XP,levelmanager.currentLevel,(int)h.healthPoints,Corazon.contCorazon);
			JsonData JSave = JsonMapper.ToJson(Save);


		File.WriteAllText((Application.dataPath + "/StreamingAssets/statsPlayer"+(tempid+1)+".json"), JSave.ToString());
	}
    
	void StarHeart(int hearts){
		for(int i =0; i<hearts ; i++){
			GameObject obj = Instantiate (levelmanager.gamePlayer.gameObject.GetComponent<TakeItems>().ObjCorazon);
			obj.GetComponent<Corazon> ().corazon = Corazon.contCorazon;
			obj.transform.SetParent (levelmanager.panelHeart.transform);
			obj.GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);
			levelmanager.panelHeart.GetComponent<RectTransform> ().sizeDelta+= new Vector2(80,0);
		}
	}

    public class dataPlayer{
		public  string nombre{ get; set;}
		public int nivel{ get; set;}
		public int x{ get; set;}
		public int y{ get; set;}
		public int z{ get; set;}
		public int money{ get; set;}
		public int xp{ get; set;}
		public int currentLevel{ get; set;}
		public int health{ get; set;}
		public int heart{ get; set;}
        public dataPlayer(){
			
		}

		public dataPlayer( string name, int nivel,int x,int y, int z,int money,int xp,int currentlevel,int health,int heart ){
            this.nombre = name;
            this.nivel = nivel;

			this.x =x;
			this.y =y;
			this.z =z;

			this.money=money;
			this.xp =xp;
			this.currentLevel=currentlevel;

			this.health=health;
			this.heart=heart;

        }
    }
}
