﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GiveQuest : MonoBehaviour {
	public string questName = string.Empty;
	public Text messageText = null;
	public string[] messages;


	void Start(){
		messageText = FindObjectOfType<QuestManager> ().txtQuest;
		messageText.transform.parent.gameObject.SetActive (false);
	}

	void OnTriggerEnter2D(Collider2D collider){
		if (!collider.CompareTag ("Player"))
			return;
		
		Quest.QUEST_STATUS status = QuestManager.GetQuestStatus (questName);
		messageText.text = messages[(int)status];
		messageText.transform.parent.gameObject.SetActive (true);
	}

	void OnTriggerExit2D(Collider2D collider){
		if (!collider.CompareTag ("Player"))
			return;
		Quest.QUEST_STATUS status = QuestManager.GetQuestStatus (questName);

		if(status == Quest.QUEST_STATUS.UNASSIGNED){
			Debug.Log(this.gameObject);
			QuestManager.SetQuestStatus (questName,Quest.QUEST_STATUS.ASSIGNED);
		}
		messageText.transform.parent.gameObject.SetActive (false);


	}

}
