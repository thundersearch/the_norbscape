﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TalkNPC : MonoBehaviour {
	public string[] Dialogos;
	LevelManager lvl;
	QuestManager quest;
	int count;
	int countdialog;
	bool show;
	// Use this for initialization
	void Start () {
		lvl = FindObjectOfType<LevelManager> ();
		quest= FindObjectOfType<QuestManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
			
	
			

		if(Input.GetKeyDown("e")&& lvl.isDialog){
			

			if (countdialog == 0) {
				show = !show;
				quest.txtQuest.text = Dialogos [countdialog];
				countdialog++;
			} else if (countdialog < Dialogos.Length) {				
				quest.txtQuest.text = Dialogos [countdialog];
				countdialog++;
			} else if (countdialog == Dialogos.Length) {
				show = !show;
				countdialog++;
				lvl.isDialog = false;
			} else {
				lvl.isDialog = false;
			}
			if (show) {
				Time.timeScale = 0;
			} else {
				Time.timeScale = 1;
			}
		}

		if (show) {
			quest.PanelQuest.SetActive (true);

		} else if (!lvl.isActivePlayer) {
			quest.PanelQuest.SetActive (false);

		} else {
			quest.PanelQuest.SetActive (false);		

		}
	
	}
	void OnTriggerEnter2D(Collider2D other){	
		if(other.tag == "Player" && count <1){
			lvl.isDialog = true;
			count++;


		}
	}
	void OnTriggerExit2D(Collider2D other){
		if(other.tag == "Player"){	
			show = false;
			lvl.isDialog = false;
			count = 0;
		}
	}
}
