﻿using UnityEngine;
using System.Collections;

public class SectionMiniMap : MonoBehaviour {
	 public SpriteRenderer mySprite;
	// Use this for initialization
	void Start () {
		 mySprite= this.gameObject.GetComponent<SpriteRenderer> ();
		mySprite.color =new Color(1f,1f,1f,0f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D collider){
		if (!collider.CompareTag ("Player"))
			return;
			mySprite.color=new Color(1f,1f,1f,1f);

	}
}
