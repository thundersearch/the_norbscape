﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;



public class ItemVenta : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler,IPointerClickHandler {
	public Item item;




	private Inventory inv;
	private Tooltip tooltip;
	private LevelManager lvl;


	void Start(){

		inv = GameObject.Find ("Inventory").GetComponent<Inventory> ();
		tooltip = inv.GetComponent<Tooltip> ();
		lvl = FindObjectOfType<LevelManager> ();

	}


	public void OnPointerEnter (PointerEventData eventData)
	{
		tooltip.Activate (item);

	}


	public void OnPointerExit (PointerEventData eventData)
	{
		tooltip.Deactivate ();
	}

	public void OnPointerClick (PointerEventData eventData){

		if (lvl.money < item.Value) {
			Debug.Log ("menor");
		} else {
			lvl.money = lvl.money - item.Value;
			inv.AddItem (item.ID);
		}




	}



}
