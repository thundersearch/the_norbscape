﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Venta : MonoBehaviour {
	public int count;
	public LayerMask layer;
	public List<int> idItems = new List<int> ();
	Inventory inv;
	GameObject panelventa;
	GameObject slotspanel;
	LevelManager lvl;
	bool show =false;
	List<GameObject>items =new List<GameObject>();
	public GameObject itemV;

	void Start () {
		inv = FindObjectOfType<Inventory> ();
		lvl = FindObjectOfType<LevelManager> ();
		panelventa= inv.panelventa;
		slotspanel = panelventa.transform.FindChild ("Slots Panel").gameObject;

	}
	
	// Update is called once per frame
	void Update () {
		if (show && !lvl.isDialog) {
			panelventa.SetActive (true);

		} else if(!lvl.isActivePlayer) {
			panelventa.SetActive (false);

		}else {
			panelventa.SetActive (false);

		}
		Collider2D obj =  Physics2D.OverlapCircle(this.transform.position,0.5f, layer);
		if(obj ){
			if(Input.GetKeyDown("e")){
				show = !show;	
				if (show) {
					Time.timeScale = 0;
				} else {
					Time.timeScale = 1;
				}
			}

		}

	}
	void OnTriggerEnter2D(Collider2D other){	
		if(other.tag == "Player" && count <1){
			inv.showv = true;
			count++;
			llenaventa ();

		}
	}
	void OnTriggerExit2D(Collider2D other){
		if(other.tag == "Player"){
			vaciaventa ();
			inv.showv = false;
			show = false;
			count = 0;
		}
	}

	void llenaventa(){
		for(int i=0;i<idItems.Count ;i++){
			Item item = inv.database.FetchItemByID (idItems[i]);
			GameObject obj = Instantiate(itemV);
			obj.transform.GetChild(0).GetComponent<ItemVenta> ().item = item;


			obj.transform.SetParent (slotspanel.transform);
			obj.GetComponent<RectTransform> ().localScale= new Vector3(1,1,1);

			obj.transform.GetChild(0).GetComponent<Image> ().sprite = item.Sprite;
			obj.transform.GetChild(0).name = item.Title;
			obj.transform.GetChild (1).GetComponent <Text>().text= item.Value.ToString();
			items.Add (obj);
		}


	}
	void vaciaventa(){
		for(int i =0; i< items.Count;i++){
			Destroy (items [i]);
		}
		items.Clear ();

	}
}
