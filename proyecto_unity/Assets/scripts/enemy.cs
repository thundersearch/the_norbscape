﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
	

	public class enemy: MonoBehaviour{

	public LayerMask players_layer;
	public float radioAgro = 2f;
	private Transform theTransform =null ;
	public float maxSpeedt  = 0;
	public float maxSpeed  = 14.0f;
	private bool facingRight = true; //posicion de la cara
	public float agroTime =0.0f;
	public float damagePoints= 1.0f;
	Rigidbody2D rigi ;
	public Transform objectToFollow= null;

	public float armor= 3f;
	public float armortemp= 0;



	private void Awake(){
		theTransform = GetComponent<Transform> ();
		rigi = this.gameObject.GetComponent<Rigidbody2D> ();
	}

	void Update(){
		


		//theTransform.position += theTransform.right * maxSpeed * Time.deltaTime;	
		if(Physics2D.OverlapCircle(theTransform.position,radioAgro,players_layer)){
			GameObject thePlayer = GameObject.FindGameObjectWithTag ("Player");
			if(thePlayer != null){
				objectToFollow = thePlayer.GetComponent<Transform>();
				agroTime = 5.0f;
			}		
		}
		if(objectToFollow != null){
			theTransform.position = Vector3.MoveTowards(theTransform.position, objectToFollow.position, maxSpeed * Time.deltaTime);
		

			if (this.transform.position.x < LevelManager.Theplayer.transform.position.x  &&!facingRight){
				Flip();
			}
			if (this.transform.position.x > LevelManager.Theplayer.transform.position.x  && facingRight){
				Flip();
			}
			
		}	


		if(agroTime <= 0){
			agroTime = 0;
			objectToFollow = null;
			return;
		}

		agroTime -= Time.deltaTime;
	}

	void OnTriggerStay2D(Collider2D Otro){
		if(Otro.tag == "Player"){
			Health health = Otro.gameObject.GetComponent<Health>();
			Ataque ataque= Otro.transform.Find("Ataque").gameObject.GetComponent<Ataque>();
			if (health== null ){			
				return;
			}
			if(health.healthPoints > 0){
				float rdano = (ataque.armor) / (5 + ataque.armor);
				float dano = (damagePoints * Time.deltaTime);
				float danoaplicado = dano - (dano * rdano);
				health.healthPoints -=danoaplicado;
				}
		}
	}



	//voltea la cara al lugar del movimiento
	void Flip(){
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}


	 
}

