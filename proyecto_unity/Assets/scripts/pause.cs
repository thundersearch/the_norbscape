﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class pause : MonoBehaviour {
	Inventory inv;
    public GameObject pauseUI;
	public bool paused = false;
	private static pause pauses  = null;
	LevelManager levelmanager;
	public static pause pausesInstance {
		get{
			if(pauses ==null){
				GameObject questObject = new GameObject ("Defaul");
				pauses = questObject.AddComponent<pause> ();
			}

			return pauses;
		}


	}

	void Awake(){
		if(pauses){
			DestroyImmediate (gameObject);
			return;
		}
		pauses = this;
		DontDestroyOnLoad (pauses);
	}
	void Start () {
        pauseUI.SetActive(false);
		levelmanager =FindObjectOfType<LevelManager> ();
		inv = FindObjectOfType<Inventory> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Pause")&& !inv.show && levelmanager.isActivePlayer)
        {
            paused = !paused;
			if(paused){
				Time.timeScale = 0;
			}else{
				Time.timeScale = 1;
			}
        }

        if (paused)
        {
            pauseUI.SetActive(true);

        }

        if (!paused)
        {
            pauseUI.SetActive(false);

        }

          
    }
    public void resume()
    {
        paused = false;
    }

	public void reiniciarNivel(){
		paused = false;
		levelmanager.LoadLevel (levelmanager.levelActual, levelmanager.gamePlayer.respawnPoint);
	}

    public void salir(){
		Time.timeScale = 1;
		int temp = Corazon.contCorazon;
		Corazon.contCorazon = 0;
		Corazon.vidaMax = 0;


		for(int i=0;i<temp;i++){
			Destroy (levelmanager.panelHeart.transform.GetChild (i).gameObject);
		}
		levelmanager.panelHeart.GetComponent<RectTransform>().sizeDelta= new Vector2(77,80);
		levelmanager.LoadLevel (0,new Vector3(0,0));
        paused = false;
    }

	public void exitGame(){
		Application.Quit ();
	}
}
